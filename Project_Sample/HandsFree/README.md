# handsfree 快速上手



## 编译

首先找到源码，这里给一个gitee的[源码](https://gitee.com/HANDS-FREE/handsfree)把它下下来，然后新建一个ROS[工作空间](http://wiki.ros.org/cn/catkin/Tutorials/create_a_workspace)，把它放进去，先别急着`catkin_make`

### 1.安装ROS

​	[教程](http://wiki.ros.org/cn/ROS/Installation),ROS安装完可跳过这一步

### 2.安装ROS常用的第三方包

 1. 这里在下好的`handsfree`包中有可以直接用的脚本，是在`Documentation/script`文件夹下，进入到此目录下，按以下指令运行

    ```
    bash ros_install_ext_cn.sh <ros版本> 如 bash ros_install_ext_cn.sh melodic
    ```

    如果你使用的是zsh那就使用"zsh"替换"bash"，感兴趣的同学也可以看看其sh文件内部是如何编写的

### 3.编译`handsfree`

`handsfree`等待安装完成之后，就可以到你建造的工作空间内进行编译

在含有“build” “devel” “src”文件夹下使用

```
catkin_make
```

​	等待编译完成

### 4. 设置HandsFree环境变量

打开用户的 *.bashrc*文件

```
gedit ~/.bashrc
```

*shell*是一个命令行解释器，是linux内核的一个外壳,负责外界与linux内核的交互。存在很多不同的 *shell* ，*bash* 就是其中之一。而 *Ubuntu* 的 *shell* 就是 *bash*。

当你打开一个 *terminal（终端）*时，操作系统会将 *terminal* 和 *shell* 关联起来，当我们在 *terminal* 中输入命令后，*shell* 就负责解释命令。

不管哪种 *shell* 都会有一个 *.bashrc* 的隐藏文件，它就相当于 shell 的配置文件，用来存储并加载你的终端配置和环境变量。

1. 找到环境变量**HANDSFREE_ROBOT_MODEL**，`mini`是机器人的型号，需要将这个值改为你机器人的型号，可以在`[mimi,stone_v2,stone_v3,giraffe]`这四种型号中选择。

   ```
   ### MODEL type [mini, stone_v2, stone_v3, giraffe]
   export HANDSFREE_ROBOT_MODEL=mini
   ```

### 5. 设置usb规则

1. 进入 *Documentation* 文件夹下，在终端运行写好的 set_usb_env.sh脚本文件

   ```
   bash set_usb_env.sh
   ```

   这个文件用于修改给系统的 *usb* 规则，因为机器人上有许多的设备。如果没有任何关于权限上和映射关系上的处理就会导致无法正常启动这些设备。

### 6.测试

安装成功后，可以使用USB连接到机器人，打开新的终端，然后执行：

```
roslaunch handsfree_hw handsfree_hw.launch
```

如果一切正常，会显示：

```
auto-starting new master
process[master]: started with pid [9562]
ROS_MASTER_URI=http://Robot:11311

setting /run_id to ffa41cce-05bd-11e8-8df0-001e64f02337
process[rosout-1]: started with pid [9575]
started core service [/rosout]
process[handsfree_hw_node-2]: started with pid [9585]
process[mobile_base/controller_spawner-3]: started with pid [9589]
process[robot_state_publisher-4]: started with pid [9591]
[ERROR] [1517317423.863958024]: hf link initialized failed, please check the hardware
[INFO] [WallTime: 1517317424.295263] Controller Spawner: Waiting for service controller_manager/load_controller
[INFO] [WallTime: 1517317424.298319] Controller Spawner: Waiting for service controller_manager/switch_controller
[INFO] [WallTime: 1517317424.301498] Controller Spawner: Waiting for service controller_manager/unload_controller
[INFO] [WallTime: 1517317424.304670] Loading controller: joint_state_controller
[INFO] [WallTime: 1517317424.351596] Loading controller: servo1_position_controller
[INFO] [WallTime: 1517317424.399306] Loading controller: servo2_position_controller
[INFO] [WallTime: 1517317424.419282] Loading controller: mobile_base_controller
[INFO] [WallTime: 1517317424.469234] Controller Spawner: Loaded controllers: joint_state_controller, servo1_position_controller, servo2_position_controller, mobile_base_controller
[INFO] [WallTime: 1517317424.479183] Started controllers: joint_state_controller, servo1_position_controller, servo2_position_controller, mobile_base_controller
```

打开新的终端，继续执行：

```
roslaunch handsfree_hw keyboard_teleop.launch
```

来遥控机器人行走。机器人能正常遥控，说明环境配置成功。



## 实机操作

参看[handsfree教程](https://wiki.hfreetech.org/docs/Tutorial/Beginner/)

## 仿真教程

这里主要分为两个部分 2D仿真stage 3D仿真 gazebo

我觉得学习仿真的最好办法就是照葫芦画瓢，你看看这些可以运行的launch是怎么调用的，调用的文件是怎么写的，自己再模仿就可以写出自己的仿真环境

### stage

先来直接运行看一下效果

```
roslaunch handsfree_stage demo_handsfree_room_stage.launch 
```

你也可以吧`demo_handsfree_room_stage.launch`换成`demo_handsfree_xuda_stage.launch`他俩差不多，只是地图不一样。

这里你会看到两个界面，一个是*stage*的界面，另一个是*rviz*的界面，这样我们就可以在*rviz*的界面进行导航,具体怎么导航请参看

[GraphicalNavigation.md](GraphicalNavigation.md)

运行完了，可以去看一下它的launch文件如何写的，在`handsfree_stage/launch`里面,主要看其调用了什么，如何调用，学习写法

最后放一下[stage教程](https://blog.csdn.net/u010918541/article/details/53982237)

### gazebo

#### 运行

先运行launch文件

```
roslaunch handsfree_gazebo demo_gazebo_mini.launch 
```

然后运行

```
roslaunch handsfree_hw keyboard_teleop.launch
```

可以控制机器人做简单的运动

若想导航则关闭`keyboard_teleop.launch`，运行下方launch

```
roslaunch handsfree_gazebo demo_move_base_amcl_mini.launch
```

老惯例，好好看看launch文件是怎么写的，以及他那些调用的文件是怎么写的

#### 建立

**1.建立world文件**

参看[教程](https://blog.csdn.net/qq_36355662/article/details/80030372)看到第四步就够了，注意地图不要建的太大，不然建图会很费劲。

将.world文件放到`handsfree_gazebo/world`文件夹下,仿照`demo_gazebo_mini.launch `自己建立一个launch文件

**2.建图**

1.在打开自己新建的launch文件基础上，打开新终端，运行gmapping建图算法

```
 roslaunch handsfree_2dnav gmapping.launch
```

2.再打开新终端，开启RVIZ可视化工具

```
 rosrun rviz rviz -d `rospack find handsfree_bringup`/rviz/gmapping.rviz
```

rviz文件也可以手动配置，选择rviz软件界面的file -> open config，手动选择`handsfree/handsfree_bringup/rviz/gmapping.rviz`文件作为其配置文件。如果正常的话，可以在RVIZ中看到机器人模型及激光雷达扫描到的可视化数据，然后进行下一步打开键盘遥控节点建图。

3.打开新终端，运行遥控节点

```
 roslaunch handsfree_hw keyboard_teleop.launch
```

利用键盘遥控机器人缓慢移动，RVIZ中显示的地图会逐渐补全，当你构建的地图范围满足需求时，就可以运行相关指令保存地图了。

4.打开新终端，运行保存地图指令

先切换到想要保存地图的文件夹（建议保存在handsfree_gazebo/map中）

然后运行保存地图的节点：

```
 rosrun map_server map_saver -f my_map
```

**3.导航**

仿照`demo_move_base_amcl_mini.launch`把自己建的图改进去，建立一个新的launch，启动这个launch文件，就可以导航了