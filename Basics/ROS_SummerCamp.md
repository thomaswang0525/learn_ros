# SUMMER CAMP — ROS

ROS（Robot Operating System），即机器人操作系统。它提供一系列程序库和工具以帮助软件开发者创建机器人应用软件。 **ROS的核心就是提供了一套针对机器人应用的进程间通讯系统，使得不同功能的模块之间可以很方便的互联。** 如果把机器人的操纵比作一棵大树。而ROS充当的就是实现各个模块（树干，树叶）之间互通的脉络。让各个模块之间可以更方便，更快捷的互联互通。

## 1. 为什么学ROS

1. 简洁好用的通讯系统，模块间轻松互联。

2. 已经有很多成熟开源的基础模块，方便开发，不必自己造轮子。

3. 良好的可视化，模拟仿真和debug用具。可视化用具：`rviz`,`rqt_`系列；模拟仿真：`stage`，`gazebo`；debug用具 `rostopic`，`rosservice`，`rosparam`等。

4. 并且针对行业来说，ROS已经逐步在与各行各业融合。



## 2. 基础通讯

这里主要讲一下ros的通讯，其主要分为三个方面 topic,service,action。

### 2.1 为什么进程间通讯尤为重要

我们在导航方面举个例子。首先是在未使用进程间通讯的情况：



<img src="pic/例子1.png"          alt="例子1" style="zoom:33%;" />            <img src="pic/例子2.png" alt="例子2" style="zoom: 33%;" />	

而在使用进程间通讯的情况下：
<img src="pic/例子3.png" alt="例子3" style="zoom:33%;" />



* 第一种情况，代码变得臃肿，牵一发而动全身。

* 第二种情况，更为模块化，调试与修改或加入新的模块更为便捷。

### 2.2 通讯方式
简述 topic，service，action
#### 2.2.1 节点
简述节点概念。任何可执行文件都是一个节点。

#### 2.2.2 topic   
topic就是话题，它分为发布者与订阅者，二者关系如图：

<img src="pic/发布者订阅者-1.png" alt="发布者订阅者-1" style="zoom:33%;" />    <img src="pic/发布者订阅者2.png" alt="发布者订阅者2" style="zoom:33%;" />

发布者开启之后，他就会根据代码将数据发布在ros空间内，而订阅者则会按一定频率实时接收信息。发布者一旦发布数据，其就会被0~多个接收者接收。信息以及频率均可自己定义。

#### 2.2.3 service

第二中通讯方式就是Service 它分为服务器与客户端，二者的通讯关系可以如图表示。

<img src="pic/服务器-客户端.png" alt="服务器-客户端" style="zoom: 33%;" />

注意编号①和②，这代表着先后顺序。服务器节点开启之后他会一直处于一个安静的等待客户端访问的状态，直到客户端对他发起请求，他才会与客户端进行信息交互。同样，一个服务器可以同时接受多个客户端的邀请，并可以将信息发送给多个客户端。

#### 2.2.4 action
action通讯机制较为复杂，它是实现了一种类似于service的请求/响应通讯机制，区别在于action带有反馈机制，用来不断向客户端反馈任务的进度，并且还支持在任务中途中止运行。下面举一个写作文的例子，如下图所示：

<img src="pic/action.png" alt="action" style="zoom:33%;" />

可以发现他是提出一个大的目标然后进行一步步的反馈，然后最终完成任务。

### 2.3 回调函数

我们的节点不可能一直等在此处接收信息，这就需要异步操作，而这个就需要使用回调函数。

一般情况下，应用程序（application program）会时常通过API调用库里所预先备好的函数。但是有些库函数（library function）却要求应用先传给它一个函数，好在合适的时候调用，以完成目标任务。这个被传入的、后又被调用的函数就称为**回调函数**（callback function）。

ROS中就是采用这个回调函数的方式获取信息，而他的“合适的时候”是在运行到`ros::spin()`或者`ros::spinOnce()`的时候，统一去调用回调函数获取信息。

## 3. 基础概念

讲一下对于ROS中的基础概念以及一些较为常用的命令行工具。

### 3.1 命名空间

ROS为了方便区分以调用节点，消息等等。它为此准备了类似电脑的命名系统，如图。
<img src="pic/rosnode.png" alt="rosnode" style="zoom: 67%;" />

当开启一部分节点之后，就可以通过`rosnode list`来对当前节点进行查看。

而对于topic与service，也同样有命名来区分它。

<img src="pic/rosservice.png" alt="rosservice" style="zoom: 51%;" /> <img src="pic/rostopic.png" alt="rostopic" style="zoom:53%;" />

为了增加命名空间的深度、便于区分，可以看出命名是类似于文件系统的叠加。

## 4. 命令行工具

[命令行工具](http://wiki.ros.org/cn/ROS/CommandLineTools)，ROS为了方便调试，给予了我们很多命令行参数。这里主要讲述几个常用典型例子。

### 4.1 catkin_make

其主要是对写好的代码进行编译，其是在目录为`build devel src`目录下进行编译。其编译依赖`CMakeLists.txt`，它是cmake的变种，加入了一些ros的东西，详细的使用可以查看[ros官方教程](http://wiki.ros.org/cn/ROS/Tutorials)。 

### 4.2 rosrun

启动自己生成的可执行文件,用法如下：

```
rosrun package executable
```

### 4.3 rostopic

rostopic它是负责管理topic，可以进行查验与发布。

```
$ rostopic 
bw    echo  find  hz    info  list  pub   type
```

分别表示 带宽，显示内容，查找（通过msg类型找到topic名称），频率，信息，topic列表，发布，类型

想要查看某一topic只需在后续加入topic名称即可。

### 4.4 rosservice

它负责管理service

```
$ rosservice 
args  call  find  info  list  type  uri 
```

分别表示 变量，发送，查找（通过srv类型找到service名称），信息，srv列表，类型，uri地址。

### 4.5 rosparam

其主要负责ros的参数管理。

```
$ rosparam 
delete  dump    get     list    load    set
```

分别表示 删除 转储 获得 列表 加载 设置。

### 4.6 launch文件

为了快速启动多个文件，以及更方便的写入多个参数，这时我们就需要引入launch文件。它使用`xml`格式将需要同时启动的一组节点罗列出来，从而达到启动多个节点的目的。

加入节点的必备属性

1. **pkg：** 表示该节点的package，相当于 `rosrun` 命令后面的第一个参数；
2. **type：** 可执行文件的名字，`rosrun` 命令的第二个参数；是否可以理解为要执行的节点
3. **name：** 该节点的名字，相当于代码中 `ros::init()` 中设置的信息，有了它代码中的名称会被覆盖。

还有一些可选属性：

+ output： 将标准输出显示在屏幕上而不是记录在日志中；
+ respawn： 请求复位，当该属性的值为 respawn="true" 时，roslaunch 会在该节点崩溃时重新启动该节点；
+ required： 必要节点，当该值为 required="true" 时，roslaunch 会在该节点终止时终止其他活跃节点；
+ 启动前缀： 在启动命令加上前缀。例如当其设置为 launch-prefix="xterm -e" 时，效果类似于 xterm -e rosrun X X 。也就是为该节点保留独立的终端；
+ ns： 在命名空间中启动节点；
+ 重映射： 使用方法 remap from="original-name(turtle/pose)"to"new-name(tim)"；
  包含其他文件include file=“path to launch file”： 在启动文件中包含其他启动文件的内容（包括所有的节点和参数），可使用如下命令使路径更为简单include file="($find package-name)/launch-file-name"。

## 5. 基础模块

### 5.1 URDF

URDF文件主要是来描述机器人模型，让其可以在仿真情况下使用。可以手动创建一些简单为模型，对于一些大的模型一般都是通过solidsworks来进行导出的。其定义了关于机器人的各个link与joint，以及其坐标系的转换。一旦调用了URDF文件，就会自动调用TF(在下文)，来持续发布tf变换。可以参看一下这个[教程](https://blog.csdn.net/newbie_001/article/details/82594511)。

### 5.2 TF

TF(TransForm)是用来表示坐标系变换的，就如同SLAM中，机器人运动变换一般用来表示同一机器人在不同时间的一个坐标变换，并一般由变换矩阵表示。而在一个机器人内部也需要很多的坐标变换，比如激光雷达与机器人身体之间坐标系变换(laser->base_link)等等，如下图所示。

<img src="pic/frames2.png" alt="frames2" style="zoom:50%;" />

而这个变换的实际意义，可以通过一个例子来说明
<img src="pic/tf变换.png" alt="tf变换" style="zoom:50%;" />

而且不止是在机器本身上，还需有一个机器人与全局坐标系(map)的一个tf变换，来表示机器人的运动。

#### 5.3 树状结构

而且由于有许多的坐标系要进行转换，TF一般将其优化成一个树状结构，可使用`rqt_tf_tree`来查看，如下图

<img src="pic/tf_frames.png" alt="tf_frames" style="zoom:67%;" />

这里要注意一个约定俗称的规则，一个健全的tf树都需要拥有**map,odom,base_footprint,base_link**之间的转换。

### 5.4 ros_control

机器人会在上位机与下位机进行大量的信息交互，而ros_control就是在ros层面对需要向下发的消息做一个集中梳理；并将传递上来的信息做一个集中发送。

<img src="pic/ros_control.png" alt="ros_control" style="zoom:67%;" />

下面我们来简单讲述一下roscontrol的流程，一个最基础的上下位机交互流程图，如下左图
<img src="pic/roscontrol1.png" alt="roscontrol1" style="zoom:50%;" />                                           
<img src="pic/roscontrol3.png" alt="roscontrol3" style="zoom: 33%;" />

这就是controller_manager的作用，

+ read():上位机到指定位置读取信息，发送给下位机。
+ update()：信息更新
+ write():下位机信息写入至上位机指定位置。

现在我们继续，update()更新的是什么呢？就是一个个controller，ROS给出了controller的基类，里面有update()虚函数需要我们完善，controller_manager会自动调用。

而他具体更新的内容是什么，或者说是read和write的“指定位置”是哪里。

<img src="pic/roscontrol2.png" alt="roscontrol2" style="zoom:50%;" /> 

答案就是hardware_interface , ROS中也提供了他的基类，controller更新的就是它，至于更新的具体内容还需我们自定义。它就是那个指定位置，下位机信息需要先传入hardware_interface，再通过update来更新到read函数中，上位机信息也是先发送至hardware_interface再通过update载入。

### 5.5 smach

有限状态机（Finite-state machine, FSM），又称有限状态自动机，简称状态机，是表示有限个状态以及在这些状态之间的转移和动作等行为的数学模型。FSM是一种算法思想，简单而言，有限状态机由一组状态、一个初始状态、输入和根据输入及现有状态转换为下一个状态的转换函数组成。

状态存储关于过去的信息，就是说：它反映从系统开始到现在时刻的输入变化。转移指示状态变更，并且用必须满足确使转移发生的条件来描述它。动作是在给定时刻要进行的活动的描述。有多种类型的动作：

+ 进入动作（entry action）：在进入状态时进行
+ 退出动作：在退出状态时进行
+ 输入动作：依赖于当前状态和输入条件进行
+ 转移动作：在进行特定转移时进行

<img src="pic/smatch.png" alt="smatch" style="zoom:50%;" />

### 5.6 mavros

它主要是对mavlink做的ros层面的封装，为了适应ros控制，并给出了相当多的接口。见[官网](http://wiki.ros.org/mavros)





## 6. 仿真介绍

### 6.1 stage

它是一个二维的轻量级仿真，可以模拟出机器人底盘，激光，摄像机，以及深度摄像机模型，还可以模拟出tf信息。而他需要接收的话题只有一个是速度。所以它是一个只能仿真机器人导航的二维仿真平台。

它的主体是world文件，通过跟随教程编写此文件，可以加载出相应地图，以及机器人模型，并为机器人构建激光雷达，摄像头等传感器。

### 6.2 gazebo

gazebo这个仿真平台就会完善很多。它是一款3D动态模拟器，能够在复杂的室内和室外环境中准确有效地模拟机器人群。与游戏引擎提供高保真度的视觉模拟类似，Gazebo提供高保真度的物理模拟，其提供一整套传感器模型，以及对用户和程序非常友好的交互方式。

Gazebo的一些主要特点：

+ 包含多个物理引擎
+ 包含丰富的机器人模型和环境库
+ 包含各种各样的传感器
+ 程序设计方便和具有简单的图形界面



它可以像导入urdf文件一样导入模型。也可以图形化制作或手动写入生成world，来创建地形，以及一些物体插件。



## 7.  图形化用具

讲述一下ros强大的图形化工具。也就是rqt系列。

```
$ rosrun rqt_
rqt_action           rqt_logger_level     rqt_robot_plugins  
rqt_bag              rqt_moveit           rqt_robot_steering 
rqt_bag_plugins      rqt_msg              rqt_runtime_monitor
rqt_common_plugins   rqt_nav_view         rqt_rviz           
rqt_console          rqt_plot             rqt_service_caller 
rqt_dep              rqt_pose_view        rqt_shell          
rqt_graph            rqt_publisher        rqt_srv            
rqt_gui              rqt_py_common        rqt_tf_tree        
rqt_gui_cpp          rqt_py_console       rqt_top            
rqt_gui_py           rqt_reconfigure      rqt_topic          
rqt_image_view       rqt_robot_dashboard  rqt_web            
rqt_launch           rqt_robot_monitor

#或者运行
$rqt

#查看plugins
```

这里主要讲述几个比较常用的rqt用具。

### 7.1 rqt_top
如下，启动类似于进程表的rqt_top的图形化工具， 可以快速查看正在使用的所有节点和资源。

```
rosrun rqt_top rqt_top

```

### 7.2 rqt_topic

如下，启动rqt_topic图形化工具，显示主题调试信息， 包括发布者、 接收者、 发布速率和发布的消息。 可以查看消息字段并选择你想要订阅的主题以分析带宽和速率（Hz），以及查看最新发布的消息。 注意， 锁定的主题通常不会持续发布， 所以不会看到任何关于它们的信息。

```
rosrun rqt_topic rqt_topic
```

### 7.3 rqt_publisher

如下，启动rqt_publisher图形化工具， 在一个界面中管理rostopic pub命令的多个实例。

```
rosrun rqt_publisher rqt_publisher 
```

### 7.4 rqt_graph

如下，启动rqt_graph图形化工具， 显示ROS会话的当前状态。

```
rosrun rqt_graph rqt_graph
```

### 7.5 rqt_reconfigure

启动rqt_reconfigure图形化工具， 显示ROS会话的当前状态。

```
rosrun rqt_reconfigure rqt_reconfigure
```

### 7.6 rqt_tf_tree

