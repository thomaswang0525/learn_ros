# ROS古月居21讲

## ROS核心概念

### 1. 通信机制

#### 节点与节点管理器

节点（Node）：执行单元

* 节点的命名必须唯一
* 执行具体的任务进程
* 可以是不同的语言，分布在不同的主机

节点管理器（ROS Master）：控制中心

* 辅助节点查找，建立连接
* 为节点提供命名和注册的服务
* 提供参数服务器（提供全局变量的配置，为各个节点提供服务）

#### 话题通信（Topic）

两种通信方式之一，**单向数据传输**，异步通信机制

* 数据流向为发布者-订阅者，发布-订阅模型
* 可以是多对多
* 话题的数据是消息
* 适用于数据传输

$$
Publisher\rightarrow Subscriber
$$

消息（Message）

* 一定的类型和数据结构
* 使用`.msg`文件定义

#### 服务（Service）

两种通信方式之一，**双向数据传输**，同步通信机制

* 客户端-服务器模型，客户端发送请求，服务器完成处理后返回应答数据
* `.srv`文件定义请求和应答的数据结构
* 适用于逻辑处理
* 一对多的节点关系

#### 参数（Parameter）

全局共享字典

* 可通过网络访问的共享且多变量字典
* 适合存储静态的非二进制的参数
* 不适合存储动态数据（ROS有动态参数配置）

### 2. 文件系统

* 功能包（Package）：ROS软件中的基本单元，包含节点源码，配置文件，数据定义等
* 功能包清单（Package Manifest）：功能包的基本信息
* 元功能包（Meta Packages）：功能包合集，多个用于同一目的的功能包

## ROS命令行工具的使用

`rosnode`

`rostopic`

`rosservice`

这三个可以查看相关命令；

```shell
$ rqt_graph
```

用于调用QT显示节点信息；

`rosbag`

* record
* play

## 创建工作空间与功能包

### 1. 工作空间（workspace）

* src：代码空间；源代码，配置文件；
* build：编译空间
* devel：开发空间
* install：安装空间

**创建工作空间**

创建workspace文件夹，在其下创建src文件夹

`catkin`是`ros`的编译公具；

在src文件夹中：

```shell
$ catkin_init_workspace
```

**编译工作空间**

回到workspace文件夹目录，编译：（在根目录编译）

```shell
$ catkin_make
```

如果要安装，需要用

```shell
$ catkin_make install
```

可以联系CMake的编译安装过程；

**设置环境变量**

```shell
$ source devel/setup.bash
```

**检查环境变量**

```shell
$ echo $ROS_PACKAGE_PATH
```

### 2. 功能包（package）

首先要切换到SRC文件中：

```shell
$ cd src/
```

注意，工作空间下不能有相同名字的功能包

功能包的创建：

```shell
$ catkin_create_pkg <package_name> [depend1] [depend2]
```

* 功能包的名字
* 功能包的依赖

功能包的编译：

```shell
$ cd ~/workspace  # 首先切换到工作空间根目录
$ catkin_make
$ source ~/workspace/devel/setup/bash
```

如果要运行某个程序，需要先设置环境变量，`setup.bash`

## 发布者Publisher的编程实现

### 1. 首先，创建功能包

### 2. 其次，创建发布者代码

* 初始化ros节点；
* 向ROS Master注册节点信息，包括发布的话题名和话题中的消息类型；
* 创建消息数据；
* 按照一定频率循环发布消息；

![publisher](images/publisher.png)



### 3. make运行

先写在clion配置好的情况下运行的方法

* Edit Configurations，改为executable的程序
* rosrun + node
* 切换到根目录，`source devel/setup.bash`
* rosrun + pkg + exe

```cpp
// init ros node
    ros::init(argc, argv, "velocity_publisher");

    // create node
    ros::NodeHandle n;

    // create a publisher
    ros::Publisher turtle_vel_pub = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 10);

    // set loop rate
    ros::Rate loop_rate(10);

    int count = 0;
    while(ros::ok()){
        // init msg Twist
        geometry_msgs::Twist vel_msg;
        vel_msg.linear.x = 0.5;
        vel_msg.angular.z = 0.2;

        // publish message
        turtle_vel_pub.publish(vel_msg);
        ROS_INFO("publish turtle velocity command[%0.2f m/s, %0.2f rad/s]",
                 vel_msg.linear.x, vel_msg.angular.z);

        // set delay
        loop_rate.sleep();
    }
```

首先，实例化一个`Publisher`对象：

```cpp
    ros::Publisher turtle_vel_pub;
```

然后在节点句柄创建完成后，定义发布话题的名称和数据类型以及队列长度：

```cpp
    // create a publisher
    // publish topic named /turtle1/cmd_vel
    // message type defined as geometry_msgs::Twist
    turtle_vel_pub = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 10);
```

最后定义消息，发送：

```cpp
            geometry_msgs::Twist vel_msg;
            vel_msg.linear.x = 0.5;
            vel_msg.angular.z = 0.2;
            turtle_vel_pub.publish(vel_msg);
```

## 订阅者的编程实现

* 初始化ROS节点
* 订阅需要的话题
* 循环等待话题消息，接收到消息后进入回调函数
* 在回调函数中完成消息处理

例子：订阅海龟的位姿信息

* 话题：/turtle1/pose
* 消息类型： turtlesim::Pose

```cpp
void poseCallback(const turtlesim::Pose::ConstPtr& msg){
    // print message received
    ROS_INFO("Turtle Pose: x: %0.6f, y: %0.6f", msg->x, msg->y);
}

int main(int argc, char **argv){
    // init ros node
    ros::init(argc, argv, "pose_subscriber");

    // create node handle
    ros::NodeHandle n;

    // create a subscriber
    // subscribe topic whose name is /turtle1/pose
    // write callback function
    ros::Subscriber pose_sub = n.subscribe("/turtle1/pose", 10, poseCallback);

    // looping, wait for callback getting data
    ros::spin();

    return 0;
}
```

* `spin()`是一个等待数据传来之后才进入回调函数，回调函数在subscriber里已经注册过了；直接ctrl+click点开`spin()`函数看；
* 回调函数的参数是一个指向消息的指针；
* 队列，保存从publisher传来的数据，如果超过10，淘汰时间戳最老的数据；
* 在main函数中，传给回调函数的是一个回调函数的函数名，实际上是指针；有点像中断，一旦有subscriber订阅的消息进来，就进入到回调函数作处理；

## 话题消息的定义和使用

完成消息的自定义和发布订阅的使用，由publisher发布，subscriber接受信息

话题消息：
$$
Publisher\rightarrow Subscriber
$$

### 1. 自定义话题消息

自定义话题消息分为以下几步：

* 定义msg文件
* 在package.xml里添加依赖
* 在CMakeList添加编译选项
* 编译

**定义`msg`文件**

* 文件名必须命名为`msg`，且放在具体功能包的目录下；别的命名会报错；
* 内容不能缩进，不能大写（有待考证）

比如下面的包含姓名，性别和年龄消息的定义：

```
string name
uint8  sex
uint8  age

uint8 unknown = 0
uint8 male    = 1
uint8 female  = 2
```

**在`package.xml`中添加功能包依赖**

注意看`xml`文件中写的例子和用法：

```xml
  <!-- Examples: -->
  <!-- Use depend as a shortcut for packages that are both build and exec dependencies -->
  <!--   <depend>roscpp</depend> -->
  <!--   Note that this is equivalent to the following: -->
  <!--   <build_depend>roscpp</build_depend> -->
  <!--   <exec_depend>roscpp</exec_depend> -->
```

对以依赖，要build+exec，即编译+执行；编译的依赖和运行的依赖可能不一样；

在该文件里已经包含了创建功能包时添加的依赖，比如roscpp，rospy，std_msgs这种基本的，如果需要添加其他依赖，就在package.xml文件修改

在这里添加两个动态产生`message`的功能包：

```xml
<build_depend>message_generation</build_depend>
<exec_depend>message_runtime</exec_depend>
```

**在`CMakeList.txt`中添加编译选项**

将`message_generation`添加到找包中：

```cmake
find_package(catkin REQUIRED COMPONENTS
  geometry_msgs
  roscpp
  rospy
  std_msgs
  turtlesim
  message_generation
)
```

声明消息文件，参考ROS官方的写法

```cmake
################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a exec_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a exec_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
# generate_messages(
#   DEPENDENCIES
#   geometry_msgs#   std_msgs
# )
```

大致对于topic，service和action三种通信机制（action是通信机制？），声明消息文件的方法是，**`add_xxx_files()+generate_messages()`**

```cmake
add_message_files(
        FILES Person.msg
)

generate_messages(
        DEPENDENCIES std_msgs
)
```

最后在`catkin_package`中添加`message_runtime`的运行依赖

```cmake
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES learning_topic
   CATKIN_DEPENDS geometry_msgs roscpp rospy std_msgs turtlesim message_runtime
#  DEPENDS system_lib
)
```

原本这行是注释掉的，因为创建功能包时已经添加好了这些依赖（我的猜想）；

需要添加其他依赖的时候，取消注释，加到后面就行了；

**编译**

```shell
$ catkin_make
```

之后在`devel/include/pkg`里面会生成`Person.h`，里面写了详细的调用方法；

### 2. Publisher and Subscriber

### 3. make

可执行文件与动态生成的程序也产生依赖

```cmake
add_executable(person_publisher src/person_publisher.cpp)
target_link_libraries(person_publisher ${catkin_LIBRARIES})
add_dependencies(person_publisher ${PROJECT_NAME}_generate_messages_cpp)

add_executable(person_subscriber src/person_subscriber.cpp)
target_link_libraries(person_subscriber ${catkin_LIBRARIES})
add_dependencies(person_subscriber ${PROJECT_NAME}_generate_messages_cpp)
```

要和自定义消息作连接，必须添加：

```cmake
add_dependencies(person_subscriber ${PROJECT_NAME}_generate_messages_cpp)
```

![person_info](images/person_info.png)

## 客户端Client的编程实现

* 初始化ROS节点
* 设置阻塞函数，直到发现`\spawn`服务时才向下进行，否则截止在该位置
* 发现服务之后创建客户端
* 设置数据
* 请求服务调用

阻塞函数`waitForService(service name)`：

```cpp
	// wait until service /spawn is founded
    ros::service::waitForService("/spawn");
```

创建客户端，连接到服务：

```cpp
    // create a client, connecting to service /spawn
    ros::ServiceClient add_turtle = n.serviceClient<turtlesim::Spawn>("/spawn");
```

设置数据和请求服务调用：

```cpp
// init request data
    turtlesim::Spawn srv;
    srv.request.x = 2.0;
    srv.request.y = 2.0;
    srv.request.name = "turtle2";

    // call request service
    ROS_INFO("call service to spawn turtle[x:%0.6f, y:%0.6f, name:%s]",
             srv.request.x, srv.request.y, srv.request.name.c_str());
    add_turtle.call(srv);
```

结果（示例）：

![client](images/client.png)

## 服务器Server的编程实现

$$
Client(Terminal)\overset{Request}{\underset{Response}{\longleftrightarrow}}Service\overset{Request}{\underset{Response}{\longleftrightarrow}}Server
$$



* Client发布request来控制Server
* Server接收指令，并且完成指令的发送
* 针对服务的标准定义，Trigger，`std_srvs::Trigger`，是一个服务数据类型

过程实现如下：

* 创建ROS节点
* 创建节点句柄
* 创建服务端，并注册回调函数`ros::ServiceServer command_service = n.advertiseService("/turtle_command", commandCallback);`
* `commandCallback`是回调函数
* 创建Publisher，发布`/turtle1/cmd_vel`的消息
* 如果收到了请求，触发了回调函数，向下进行；否则，循环等待回调函数；
* 回调函数中改变标记位的bool值，然后发送反馈信息

### 1. 回调函数定义

```cpp
bool commandCallback(std_srvs::Trigger::Request &req,
                     std_srvs::Trigger::Response &res){
    // use as flag
    pubCommand = !pubCommand;

    // show request data
    ROS_INFO("publish turtle velocity command [%s]", pubCommand == true? "yes":"no");

    // set feedback data
    res.success = true;
    res.message = "Changed turtle command state...";

    return true;
}
```

注意`std_srv/Trigger`的结构，可以使用`rossrv show std_srvs/Trigger`命令查看：

```shell
---
bool success
string message
```

可以看到包含两个数据，在这里`Request`没用到，因为只是发送，没有请求；

### 2. Server

```cpp
    // create a server named /turtle_command
    // define callback function "commandCallback"
    ros::ServiceServer command_service = n.advertiseService("/turtle_command", commandCallback);
```

### 3. make

在这里详细地写一遍；

首先打开ros：

```shell
$ roscore
```

其次进行编译（切换到工作空间根目录）：

```shell
$ cd xxx
$ catkin_make
```

然后**更新环境变量**：

```shell
$ source devel/setup.bash
```

如果在`.bashrc`里修改过了，就很方便；

并且运行相关节点，比如这个示例用的是`turtle`：

```shell
$ rosrun turtlesim turtlesim_node
```

最后运行相关功能包里面编译生成的可执行文件

```shell
$ rosrun [package name] [exe name]
```

## 服务数据的定义和使用

比如`spin()`和`Trigger`，就是服务数据的类型；

可以回想`.msg`的定义；
$$
Client(Terminal)\overset{Request}{\underset{Response}{\longleftrightarrow}}Service\overset{Request}{\underset{Response}{\longleftrightarrow}}Server
$$

### 1. 自定义服务数据

**首先定义`srv`文件**

由于数据分为`request`和`response`两部分，需要用`---`分隔

首先在功能包的目录下新建`srv`文件夹；

其次新建`srv`文件

```shell
$ touch Person.srv
```

最后编辑`srv`文件

**在`package.xml`文件中添加功能包依赖**

在这里添加两个动态产生`message`的功能包：

```xml
<build_depend>message_generation</build_depend>
<exec_depend>message_runtime</exec_depend>
```

**在`CMakeList.txt`中添加编译选项**

首先，将`message_generation`添加到找包中：

```cmake
find_package(catkin REQUIRED COMPONENTS
  geometry_msgs
  roscpp
  rospy
  std_msgs
  turtlesim
  message_generation
)
```

其次，添加服务文件

```cmake
add_service_files(FILES [filename])
generate_messages(DEPENDENCIES std_msgs) # create .h file
```

最后在`catkin_package`中添加`message_runtime`的运行依赖

```cmake
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES learning_topic
   CATKIN_DEPENDS geometry_msgs roscpp rospy std_msgs turtlesim message_runtime
#  DEPENDS system_lib
)
```

**编译**

```shell
$ catkin_make
```

之后在`devel/include/pkg`里面会生成`Person.h`，里面写了详细的调用方法；还生成了两个request和response的头文件；

**ERROR Recording!**

生成的`srv`在编译之后，在`devel/include/package_name`里生成`.h`头文件；

但是在`.cpp`文件里，`#include "package_name/Person.h"`报错；

我打开一看，发现功能包的src下面的include里面确实一个文件没有，但是`devel/include/package_name`里有`.h`头文件；而且另一个功能包完全可以找到文件；

最后发现新建的功能包少了`.catkin`，因为在clion配置时，我的`CMake Options`写成了：

 `DCATKIN_DEVEL_PREFIX:PATH=<WORKSPACE_DIRECTORY>/devel`

实际上却是：

 `-DCATKIN_DEVEL_PREFIX:PATH=<WORKSPACE_DIRECTORY>/devel`，少了个`-`

太头疼了这个bug...比对了好久

**CMakeList**

添加动态生成cpp的依赖

```cmake
add_executable(person_client src/person_client.cpp)
target_link_libraries(person_client ${catkin_LIBRARIES})
add_dependencies(person_client ${PROJECT_NAME}_gencpp)

add_executable(person_server src/person_server.cpp)
target_link_libraries(person_server ${catkin_LIBRARIES})
add_dependencies(person_server ${PROJECT_NAME}_gencpp)
```

注意此处和话题消息添加的依赖不一样。

### 2. Client and Server

Client:

```cpp
    // wait for service "/show_person"
    // then create a new client, connect it
    ros::service::waitForService("/show_person");
    ros::ServiceClient person_client = n.serviceClient<learning_service::Person>("/show_person");

    // init request data of learning_service::Person
    learning_service::Person srv;
    srv.request.name = "Tom";
    srv.request.age = 20;
    srv.request.sex = learning_service::Person::Request::male;

    // call request
    ROS_INFO("Call service to show person[name:%s, age:%d, sex:%d]",
             srv.request.name.c_str(), srv.request.age, srv.request.sex);
    person_client.call(srv);

    // show calling result
    ROS_INFO("show person result: %s", srv.response.result.c_str());
```

Server:

```cpp
    // create a server named "/show_person"
    // define callback function personCallback
    ros::ServiceServer person_service = n.advertiseService("/show_person", personCallback);

    // loop, waiting for callback function
    ROS_INFO("ready to show person information");
    ros::spin();
```

### 3. make

```shell
$ catkin_make
```

要注意环境变量

```shell
$ rosrun learning_service person_client
$ rosrun learning_service person_server
```

![service](images/service.png)

## 参数的使用与编程方法

参数模型：全局字典，保存一些全局配置参数；简单理解为全局变量的存储空间；

创建新的功能包：

```shell
$ catkin_create_pkg learning_parameter roscpp rospy std_srvs
```

### 1. `rosparam`

使用`rosparam`列出配置参数：

```shell
$ rosparam list
```

获得配置参数：

```shell
$ rosparam get [param]
```

修改配置参数，使用service更新：

```shell
$ rosparam set [param] [value]
$ rosservice call /clear "{}"
```

保存参数（保存到当前终端的路径）：

```shell
$ rosparam dump [file name].yaml
```

加载配置文件：

```shell
$ rosparam load [file name].yaml
```

删除参数：

```shell
$ rosparam delete [param name]
```

### 2. 编程实现

```cpp
    // get background RGB param
    int red = 0, green = 0, blue = 0;
    ros::param::get("/turtlesim/background_r", red);
    ros::param::get("/turtlesim/background_g", green);
    ros::param::get("/turtlesim/background_b", blue);
```

## ROS中的坐标系管理系统

### 1. `tf`功能包

相当于封装了底层的矩阵变换；

* 广播TF变换
* 监听TF变换

安装示例功能包：

```shell
$ sudo apt-get install ros-melodic-turtle-tf
```

一般已经安装（都是desktop-full）；

启动海龟跟随脚本：

```shell
$ roslaunch turtle_tf  turtle_tf_demo.launch
```

三种可视化方式：view_frames，echo，rviz

**使用`view_frames`查看坐标系连接：**

```shell
$ rosrun tf view_frames
```

会生成一个pdf，路径在当前终端路径下；



![frame](images/frame.png)

* world是世界坐标系
* 可以查看tf是不是连通，坐标系之间的关系是不是建立成功

**使用`rosrun tf tf_echo [axis1] [axis2]`查看两个坐标系之间的变换关系：**

```shell
$ rosrun tf tf_echo turtle1 turtle2
```

![echo](images/echo.png)

* 内容包含平移和旋转矩阵
* 旋转矩阵用了三种方式描述，有四元数和普通的旋转向量

**使用RViz：**

```shell
$ rosrun rviz rviz -d `rospack find turtle_tf` /rviz/turtle_rviz.rviz
```

![rviz](images/rviz.png)

### 2. 广播与监听的编程实现

**如何实现一个tf广播器：**

* 定义TF广播器（Transform Broadcaster）
* 创建坐标变换值
* 发布坐标变换（send Transform）

定义tf广播器：

```cpp
    // create tf broadcaster
    static tf::TransformBroadcaster br;
```

创建坐标变换值：

```cpp
    // init tf data
    tf::Transform  transform;
    transform.setOrigin(tf::Vector3(msg->x, msg->y, 0.0));
    tf::Quaternion q;
    q.setRPY(0, 0, msg->theta);
    transform.setRotation(q);
```

发布坐标变换：

```cpp
    // broadcast tf data between world and turtle(store in transform)
    // describe axis relationship of world and turtle_name
    // from now to next 10 seconds
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", turtle_name));
```

**如何实现一个tf监听器：**

* 定义TF监听器
* 查找坐标变换

定义监听器：

```cpp
// create tf listener
    tf::TransformListener transformListener;
```

查找坐标变换：

```cpp
	tf::StampedTransform transform;            
	transformListener.waitForTransform("/turtle2", "/turtle1", ros::Time(0), ros::Duration(3.0));
    transformListener.lookupTransform("/turtle2", "/turtle1", ros::Time(0), transform);
```

### 3. make

[ros的重映射](https://zhuanlan.zhihu.com/p/80366497)

```shell
$ rosrun learning_tf turtle_tf_broadcaster __name:=turtle1_tf_broadcaster /turtle1
$ rosrun learning_tf turtle_tf_broadcaster __name:=turtle2_tf_broadcaster /turtle2
```

` __name:turtle1_tf_broadcaster`会取代掉程序中定义的节点名字，但是功能是一样的，这样同样的程序可以运行两次；

最后查询两个坐标系之间的关系，给`turtle2`发送速度指令使其移动；

```shell
$ rosrun learning_tf turtle_tf_listener
```

也可以使用键盘控制，来观察一下运动跟随情况；

![tf_result_show](images/tf.png)

## `launch`启动文件的使用方法

快速启动指令，节点；通过XML文件实现多节点的配置和启动；

且`launch`可自动启动`roscore`；

### 1. 语法

**launch和node：**

```xml
<launch>
    <node pkg="your package name" name="your node name" type="your node type"/>
    <node pkg="your package name" name="your node name" type="your node type"/>
</launch>
```

* pkg：节点所在的功能包名称
* type：节点的可执行文件名称
* name：节点运行时的名称，相当于程序中`ros::init()`第三个参数；

**参数设置：**

```xml
<param name="param name" value="param value"/>
```

* 设置ROS运行中的参数，存储在参数服务器中

```xml
<rosparam file="filename.yaml" command="load" ns="params"/>
```

* 加载文件中的多个参数，存储在参数服务器

```xml
<arg name="arg name" default="arg value"/>
```

* arg（argument）仅限launch文件内部的局部变量，仅限于launch文件使用
* 参数分别是参数名和参数值

arg的调用：

```xml
<param name="arg name" value="$(arg arg_name)"/>
```

**重映射remap：**

```xml
<remap from="exist name" to="new name"/>
```

* 把原来的命名改为现在的命名
* 原来的命名则不复存在

**嵌套include：**

```xml
<include file="$(path)/name.launch"/>
```

* 用于包含其他launch文件

具体信息参考官网http://wiki.ros.org/roslaunch/XML

### 2. 例子

首先新建功能包（也可以直接用现有的）；

在src目录下新建`launch`文件夹，里面存放具体的launch文件；

编写launch文件；

运行；

```shell
$ catkin_make
$ source devel/setup.bash
$ roslaunch learning_launch simple.launch
```

simple.launch如下：

```xml
<launch>
    <node pkg="learning_topic" type="person_subscriber" name="talker" output="screen"/>
    <node pkg="learning_topic" type="person_publisher"  name="listener" output="screen"/>
</launch>
```

turtlesim_parameter_config.launch如下：

```xml
<launch>

	<param name="/turtle_number" value="2"/>

	<node pkg="turtlesim" type="turtlesim_node" name="turtlesim_node">
		<param name="turtle_name1" value="Tom"/>
		<param name="turtle_name2" value="Jerry"/>

		<rosparam file="$(find learning_launch)/config/param.yaml" command="load"/>
	</node>

	<node pkg="turtlesim" type="turtle_teleop_key" name="turtle_teleop_key" output="screen"/>

</launch>
```

* 运行之后通过`rosparam list`命令，可以查看到参数管理器里面新增的参数；
* param写到node内部，则沿用node的命名空间
* param写到node外部，有自己的命名空间
* 因此可能有多个命名空间

start_tf_demo.launch

```xml
<launch>
    <!-- turtlesim node -->
    <node pkg="turtlesim" type="turtlesim_node" name="sim"/>
    <node pkg="turtlesim" type="turtle_teleop_key" name="teleop" output="screen"/>
    
    <node pkg="learning_tf" type="turtle_tf_broadcast" args="/turtle1" name="turtle1_tf_broadcaster"/>
    <node pkg="learning_tf" type="turtle_tf_broadcast" args="/turtle2" name="turtle2_tf_broadcaster"/>
    
    <node pkg="learning_tf" type="turtle_tf_listener" name="listener"/>

</launch>
```

## qt工具箱的可视化工具

最简单的方法`rqt+Tab`

