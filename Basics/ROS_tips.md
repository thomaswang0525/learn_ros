# ROS使用、开发技巧



## 1. 开发环境设置

### 1.1 QtCreator配置
可以参考 [beginner_tutorials](workspace_example/src/beginner_tutorials) 示例程序。

在 `CMakeLists.txt`文件中，加入下面的内容。注意：`/opt/ros/melodic`根据自己的系统进行调整
```
set(CMAKE_PREFIX_PATH /opt/ros/melodic)
```

### 1.2 VS Code + ROS配置

参考：[VScode配置ROS开发环境](https://blog.csdn.net/qq_31918901/article/details/111474875?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-1.pc_relevant_paycolumn_v2&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-1.pc_relevant_paycolumn_v2&utm_relevant_index=1)

其实google直接搜VS Code ROS配置，出来的东西都差不多，就用这个分析；

需要在工作空间下打开`code`

```shell
$ code.
```

需要配置c_cpp_properties.json,launch.json,tasks.json

**`c_cpp_properties.json`**

```json
           "includePath": [
                "/usr/local/",
                "/usr/include/",
                "/opt/ros/melodic/include/"
            ],
			"compileCommands": "${workspaceFolder}/build/compile_commands.json",
             "browse": {
                	"path": [
                    "/usr/local/*",
                    "/usr/include/*",
                    "/opt/ros/melodic/include/*",
                    "/opt/ros/melodic/include/ros/*"
                ]
            }
```

大多的教程都提的加上

```json
			"compileCommands": "${workspaceFolder}/build/compile_commands.json",
```

但是我的出问题，还是找不到ros的路径，后来加上了`includePath`，还是不行，最后把编译器换了，可以了；

原本VS Code默认的编译器是GNU，但是上面的连接[VScode配置ROS开发环境](https://blog.csdn.net/qq_31918901/article/details/111474875?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-1.pc_relevant_paycolumn_v2&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-1.pc_relevant_paycolumn_v2&utm_relevant_index=1)给的是gcc和clang-arm64，没想到改了这个之后，居然可以了；

考虑到VS Code是一个轻量级编辑器，而Clion比较吃内存，所以优先使用VS Code；

Clion可以参考：[Clion + ROS配置官网教程](https://www.jetbrains.com/help/clion/ros-setup-tutorial.html)；

build文件夹下生成compile_commands.json文件:

```shell
$ catkin_make -DCMAKE_EXPORT_COMPILE_COMMANDS=1
```

**`tasks.json`**

```json
{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "catkin_make", //代表提示的描述性信息
            "type": "shell",  //可以选择shell或者process,如果是shell代码是在shell里面运行一个命令，如果是process代表作为一个进程来运行
            "command": "catkin_make",//这个是我们需要运行的命令
            "args": [],//如果需要在命令后面加一些后缀，可以写在这里，比如-DCATKIN_WHITELIST_PACKAGES=“pac1;pac2”
            "group": {"kind":"build","isDefault":true},
            "presentation": {
                "reveal": "always"//可选always或者silence，代表是否输出信息
            },
            "problemMatcher": "$msCompile"
        },
    ]
}
```

**`launch.json`**

### 1.3 Clion + ROS配置

既然都是CMake，也可以用直接支持CMake的Clion作为IDE

经过很久解决了Clion的bug之后发现，clion的配置似乎要更加快捷一点；

一定要以官网的教程为参考！！！[jetbrains ros-setup-tutorial](https://www.jetbrains.com/help/clion/ros-setup-tutorial.html#set-build-paths)

其中两个最核心的步骤

* 让clion拥有ros workspace的环境变量
* 更改CMake options

其中第一步，需要先更新环境变量，并且在同一个窗口中启动clion（直接点图标启动另有方法）

```shell
$ source devel/setup.bash
$ sh <PATH_TO_CLION>clion.sh
```

如果找不到clion.sh的位置，可以查找

```shell
$ locate clion.sh
```

由于我是snap安装的，直接打开snap的文件发现文件夹为空，ctrl+H也没用，所以只能这么找；大致如下：

```shell
$ sh /snap/clion/178/bin/clion.sh
```

第二步，注意看官网的教程，选择一个CMakeList打开，且必须是功能包下的，不能是src下的CMakeList

- In **Build directory**, set **<WORKSPACE_DIRECTORY>/build**.

这一步，我的会报错的，因为build已经作为src的dir了

- In **CMake options**, add `-DCATKIN_DEVEL_PREFIX:PATH=<WORKSPACE_DIRECTORY>/devel`.

切记，路经里没有`<>`符号

至此，配置完成，看起来比VS Code要简单一些。

**ERROR! Recording**

再次打开的时候

```shell
$ source <WORKSPACE_PATH>/devel/setup.bash
$ sh <PATH_TO_CLION>/bin/clion.sh
```

出现了python兼容版本的错误：`safe_execute_process.cmake:11`，网上的解释为https://www.codeleading.com/article/14894317433/

```
ImportError: No module named catkin.environment_cache
CMake Error at /opt/ros/melodic/share/catkin/cmake/safe_execute_process.cmake:11 (message):
  execute_process(/usr/bin/python2
  "/home/hazyparker/project/learn_ros/Basics/test2_ws/src/learning_topic/build/catkin_generated/generate_cached_setup.py")
  returned error code 1
```

我电脑默认的python版本确实不是ROS默认的2.7（我调成了3.6.9，忘了什么地方默认的2.7不行，于是我就更改了默认的python版本），但我首先没有去更改这个python版本，而是采用了下面这种解法： https://blog.csdn.net/qq_45616304/article/details/108992661 ，即

```shell
$ source /opt/ros/melodic/setup.bash
$ sh <PATH_TO_CLION>/bin/clion.sh
```