#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/hazyparker/project/learn_ros/Basics/test2_ws/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/hazyparker/project/learn_ros/Basics/test2_ws/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/hazyparker/project/learn_ros/Basics/test2_ws/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PYTHONPATH="/home/hazyparker/project/learn_ros/Basics/test2_ws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES='/home/hazyparker/project/learn_ros/Basics/test2_ws/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/hazyparker/project/learn_ros/Basics/test2_ws/src:/home/hazyparker/project/learn_ros/Basics/test2_ws/src/learning_topic:/home/hazyparker/learn_ros/Basics/test2_ws/src:/home/hazyparker/project/learn_ros/Basics/test2_ws/src/learning_service:$ROS_PACKAGE_PATH"