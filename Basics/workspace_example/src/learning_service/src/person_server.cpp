//
// Created by hazyparker on 2022/1/7.
//

#include <ros/ros.h>
#include "learning_service/Person.h"

bool personCallback(learning_service::Person::Request &req,
                    learning_service::Person::Response &res){
    // show request data
    ROS_INFO("Person: name:%s  age:%d  sex:%d", req.name.c_str(), req.age, req.sex);

    // set feedback data
    res.result = "data flow succeed!";

    return true;
}

int main(int argc, char **argv){
    // ros node init
    ros::init(argc, argv, "person_server");

    // create ros node
    ros::NodeHandle n;

    // create a server named "/show_person"
    // define callback function personCallback
    ros::ServiceServer person_service = n.advertiseService("/show_person", personCallback);

    // loop, waiting for callback function
    ROS_INFO("ready to show person information");
    ros::spin();

    return 0;
}
