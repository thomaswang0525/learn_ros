//
// Created by hazyparker on 2022/1/3.
// request /spawn service, type as turtle sim::Spawn

#include <ros/ros.h>
#include <turtlesim/Spawn.h>

int main(int argc, char **argv){
    // init ROS node
    ros::init(argc, argv, "turtle_spawn");

    // create node handle
    ros::NodeHandle n;

    // wait until service /spawn is founded
    ros::service::waitForService("/spawn");
    // create a client, connecting to service /spawn
    ros::ServiceClient add_turtle = n.serviceClient<turtlesim::Spawn>("/spawn");

    // init request data
    turtlesim::Spawn srv;
    srv.request.x = 2.0;
    srv.request.y = 2.0;
    srv.request.name = "turtle2";

    // call request service
    ROS_INFO("call service to spawn turtle[x:%0.6f, y:%0.6f, name:%s]",
             srv.request.x, srv.request.y, srv.request.name.c_str());
    add_turtle.call(srv);

    // show result of calling service
    ROS_INFO("Spawn turtle successfully [name:%s]", srv.response.name.c_str());

    return 0;
}

