//
// Created by hazyparker on 2022/1/7.
// request service /show_person, type defined as learning_service::Person

#include <ros/ros.h>
#include "learning_service/Person.h"

int main(int argc, char **argv){
    // init ros node
    ros::init(argc, argv, "person_client");

    // create ros node
    ros::NodeHandle n;

    // wait for service "/show_person"
    // then create a new client, connect it
    ros::service::waitForService("/show_person");
    ros::ServiceClient person_client = n.serviceClient<learning_service::Person>("/show_person");

    // init request data of learning_service::Person
    learning_service::Person srv;
    srv.request.name = "Tom";
    srv.request.age = 20;
    srv.request.sex = learning_service::Person::Request::male;

    // call request
    ROS_INFO("Call service to show person[name:%s, age:%d, sex:%d]",
             srv.request.name.c_str(), srv.request.age, srv.request.sex);
    person_client.call(srv);

    // show calling result
    ROS_INFO("show person result: %s", srv.response.result.c_str());

    return 0;
}


