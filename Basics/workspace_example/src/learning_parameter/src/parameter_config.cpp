//
// Created by hazyparker on 2022/1/8.
// set/get parameters in turtle node

#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <string>

int main(int argc, char **argv){
    // init ros node
    ros::init(argc, argv, "parameter_config");

    // create node handle
    ros::NodeHandle n;

    // get background RGB param
    int red = 0, green = 0, blue = 0;
    ros::param::get("/turtlesim/background_r", red);
    ros::param::get("/turtlesim/background_g", green);
    ros::param::get("/turtlesim/background_b", blue);

    ROS_INFO("get param, background color in RGB [%d, %d, %d]", red, green, blue);

    // set background RGB param
    // set all params equal 255, namely white background
    ros::param::set("/turtlesim/background_r", 255);
    ros::param::set("/turtlesim/background_g", 255);
    ros::param::set("/turtlesim/background_b", 255);

    ROS_INFO("new background color set!");

    // get params again to see changes
    ros::param::get("/turtlesim/background_r", red);
    ros::param::get("/turtlesim/background_g", green);
    ros::param::get("/turtlesim/background_b", blue);

    ROS_INFO("get param, background color in RGB [%d, %d, %d]", red, green, blue);

    // call service to refresh background color
    ros::service::waitForService("/clear");
    ros::ServiceClient clear_background = n.serviceClient<std_srvs::Empty>("/clear");
    std_srvs::Empty srv;
    clear_background.call(srv);

    sleep(1);

    return 0;
}

