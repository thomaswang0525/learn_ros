# ROS beginner tutorials

[ROS tutorial for beginner](http://wiki.ros.org/cn/ROS/Tutorials)

Copy this folder to your `~/catkin_ws/src`


## turtlesim
Detail instruction: [理解ROS话题](http://wiki.ros.org/cn/ROS/Tutorials/UnderstandingTopics)


## Usage

Compile code:
```
cd ~/catkin_ws
catkin_make
```

Run roscore
```
roscore
```

Run publisher & subscriber
```
rosrun beginner_tutorials listener
rosrun beginner_tutorials listener.py

rosrun beginner_tutorials talker
rosrun beginner_tutorials talker.py
```

Run action client & server
```
rosrun beginner_tutorials add_two_ints_client 21 3
rosrun beginner_tutorials add_two_ints_client.py 21 3

rosrun beginner_tutorials add_two_ints_server
rosrun beginner_tutorials add_two_ints_server.py
```


## Frequently used command

Change to given folder:
```
roscd beginner_tutorials
```

change to `~/catkin_ws/src/beginner_tutorials`


Show message:
```
rosmsg show beginner_tutorials/Num
```

Show service:
```
rossrv show beginner_tutorials/AddTwoInts
```

List topics:
```
rostopic list -v
```
