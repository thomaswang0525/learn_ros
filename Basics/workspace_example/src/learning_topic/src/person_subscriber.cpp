//
// Created by hazyparker on 2022/1/3.
// Subscribe topic /person_info, type defined as learning_topic::Person

#include <ros/ros.h>
#include "learning_topic/Person.h"

void personInfoCallback(const learning_topic::Person::ConstPtr &msg){
    // while message received, step to callback function
    // print message
    ROS_INFO("subscribe person info: name %s  age: %d  sex: %d",
             msg->name.c_str(), msg->age, msg->sex);
}

int main(int argc, char **argv){
    // init ROS node
    ros::init(argc, argv, "person_subscriber");

    // create node handle
    ros::NodeHandle n;

    // create a subscriber
    // subscribe topic named /person_info
    // define callback function personInfoCallback
    ros::Subscriber person_info_sub = n.subscribe("/person_info", 10, personInfoCallback);

    // wait for message
    ros::spin();

    return 0;
}

