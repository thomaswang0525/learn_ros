//
// Created by hazyparker on 2021/12/31.
//

#include <ros/ros.h>
#include "turtlesim/Pose.h"

void poseCallback(const turtlesim::Pose::ConstPtr& msg){
    // print message received
    ROS_INFO("Turtle Pose: x: %0.6f, y: %0.6f", msg->x, msg->y);
}

int main(int argc, char **argv){
    // init ros node
    ros::init(argc, argv, "pose_subscriber");

    // create node handle
    ros::NodeHandle n;

    // create a subscriber
    // subscribe topic whose name is /turtle1/pose
    // write callback function
    ros::Subscriber pose_sub = n.subscribe("/turtle1/pose", 10, poseCallback);

    // looping, wait for callback getting data
    ros::spin();

    return 0;
}

