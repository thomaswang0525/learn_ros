//
// Created by hazyparker on 2022/1/3.
// Publish person_info topic, message type defined as learning_topic::Person

#include <ros/ros.h>
#include "learning_topic/Person.h"

int main(int argc, char **argv){
    // init ROS node
    ros::init(argc, argv, "person_publisher");

    // create node handle
    ros::NodeHandle n;

    // create a publisher
    // whose topic name is person_info
    // message type is learning_topic::Person
    ros::Publisher person_info_pub = n.advertise<learning_topic::Person>("/person_info", 10);

    // set frequency for loop
    ros::Rate loop_rate(1);

    while(ros::ok()){
        // init learning_topic::Person message
        learning_topic::Person person_msg;
        person_msg.name = "Alex";
        person_msg.sex = learning_topic::Person::male;
        person_msg.age = 22;

        // publish message
        person_info_pub.publish(person_msg);

        ROS_INFO("publish person info: name: %s  age: %d  sex: %d",
                 person_msg.name.c_str(), person_msg.age, person_msg.sex);

        // set delay time for each loop
        loop_rate.sleep();
    }

    return 0;
}



