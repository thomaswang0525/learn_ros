//
// Created by hazyparker on 2021/12/31.
//

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

int main(int argc, char **argv){
    // init ros node
    ros::init(argc, argv, "velocity_publisher");

    // create node
    ros::NodeHandle n;

    // create a publisher
    ros::Publisher turtle_vel_pub = n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 10);

    // set loop rate
    ros::Rate loop_rate(10);

    while(ros::ok()){
        // init msg Twist
        geometry_msgs::Twist vel_msg;
        vel_msg.linear.x = 0.5;
        vel_msg.angular.z = 0.2;

        // publish message
        turtle_vel_pub.publish(vel_msg);
        ROS_INFO("publish turtle velocity command[%0.2f m/s, %0.2f rad/s]",
                 vel_msg.linear.x, vel_msg.angular.z);

        // set delay
        loop_rate.sleep();
    }

    return 0;
}