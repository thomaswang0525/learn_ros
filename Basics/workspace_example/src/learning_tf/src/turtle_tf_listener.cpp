//
// Created by hazyparker on 2022/1/8.
// listener of tf data

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <turtlesim/Spawn.h>


int main(int argc, char **argv){
    // init ros node
    ros::init(argc, argv, "my_tf_listener");

    // create ros node
    ros::NodeHandle n;

    // create turtle2
    ros::service::waitForService("/spawn");
    ros::ServiceClient add_turtle = n.serviceClient<turtlesim::Spawn>("/spawn");
    turtlesim::Spawn srv;
    add_turtle.call(srv);

    // create publisher
    ros::Publisher turtle_vel = n.advertise<geometry_msgs::Twist>("/turtle2/cmd_vel", 10);

    // create tf listener
    tf::TransformListener transformListener;

    ros::Rate loop_rate(10.0);
    while(n.ok()){
        // get tf data between turtle1 and turtle2
        tf::StampedTransform transform;
        try{
            transformListener.waitForTransform("/turtle2", "/turtle1", ros::Time(0), ros::Duration(3.0));
            transformListener.lookupTransform("/turtle2", "/turtle1", ros::Time(0), transform);
        }
        catch(tf::TransformException &exception){
            ROS_ERROR("%s", exception.what());
            ros::Duration(1.0).sleep();
            continue;
        }

        // publish cmd_vel of turtle2 to catch up with turtle1
        // a simple P(proportion) control
        geometry_msgs::Twist vel_msg;
        vel_msg.angular.z = 4.0 * atan2(transform.getOrigin().y(), transform.getOrigin().x());
        vel_msg.linear.x = 0.5 * sqrt(pow(transform.getOrigin().x(), 2) +
                                    pow(transform.getOrigin().y(), 2));
        turtle_vel.publish(vel_msg);

        loop_rate.sleep();
    }

    return 0;
}

