# ROS基本学习

## 教程
* [【ROS基本教程】](http://wiki.ros.org/cn/ROS/Tutorials) （官方教程）
    * 基本概念
    * 基本操作
    * Publish/Subscriber/Service
    * rosbag
* [【ROS通信基本原理】](ROS_comm.md)
    * 节点、Topic、Service、Action的概念解释
* [【ROS21讲】](ROS_basic.md)
    * ROS核心概念
    * ROS命令行工具的使用
    * 创建工作空间与功能包
    * 发布者Publisher的编程实现
    * 订阅者的编程实现
    * 话题消息的定义和使用
    * 客户端Client的编程实现
    * 服务器Server的编程实现
    * 服务数据的定义和使用
    * 参数的使用与编程方法
    * ROS中的坐标系管理系统
    * `launch`启动文件的使用方法
    * qt工具箱的可视化工具
* [【ROS SummerCamp】](ROS_SummerCamp.md)
* [【ROS开发技巧】](ROS_tips.md)
    * 开发环境设置



## 示例程序

* [beginner_tutorials](workspace_example/src/beginner_tutorials)
* [learning_topic](workspace_example/src/learning_topic)
* [learning_service](workspace_example/src/learning_service)
* [learning_parameter](workspace_example/src/learning_parameter)
* [learning_launch](workspace_example/src/learning_launch)
* [learning_tf](workspace_example/src/learning_tf)

